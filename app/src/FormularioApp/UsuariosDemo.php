<?php

namespace App\FormularioApp;
use App\Entity\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuariosDemo extends AbstractType{

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre');
        $builder->add('mail');
        $builder->add('ciudad', ChoiceType::class,
    [
        'choices' => [
            'Madrid' => 'Madrid',
            'Nueva York' => 'Nueva York',
            'Londres' => 'Londres',
            'Coria' => 'Coria',
            'Torrejoncillo' => 'Torrejoncillo',
        ]
    ]);
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            
        [
            'data_class' => Form::class
        ]
            
        );
    }
}

?>

