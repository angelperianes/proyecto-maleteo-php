<?php

namespace App\Controller;

use App\Entity\Opiniones;
use App\Entity\Form;
use App\FormularioApp\FormularioCommentsType;
use App\FormularioApp\UsuariosDemo;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MaleteoController extends AbstractController
    
{

    /**
     * @Route("/admin", name="listar_usuarios")
     */
    public function listarUsuarios(EntityManagerInterface $em)
    {
        $repositorio = $em->getRepository(Form::class);
        $usuarios= $repositorio->findAll();

        return $this->render(
            'admin.html.twig',
            [
                'usuarios' => $usuarios
            ]

        );

    }

    /**
     * @Route("/", name="homepage")
     */
    public function nuevoUsuario(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(UsuariosDemo::class);
        $form->handleRequest($request);
        $repositorio = $em->getRepository(Opiniones::class);

        $opiniones = $repositorio->findAll();

        if ($form->isSubmitted() && $form->isValid()) {
            $usuarios = $form->getData();
            $em->persist($usuarios);
            $em->flush();

            return $this->redirectToRoute('registro_confirmado');

        }

        return $this->render(
            'maleteoIndex.html.twig',
            [
                'formulario' => $form->createView(),

                'comments' => $opiniones
            ]
        );
    }

    /**
     * @Route("/admin", methods={"POST"}, name="registro_confirmado")
     */
    public function registroConfirmado(EntityManagerInterface $em)
    {
        $repositorio = $em->getRepository(Opiniones::class);
        $opiniones = $repositorio->findAll()();

        return $this->render(
            'registroConfirmado.html.twig',
            [
                'opiniones' => $opiniones
             ]

        );
    }

    /**
     * @Route("/comentarios", name="comentarios")
     */
    public function comentarios(Request $request, EntityManagerInterface $em){

       $comentarios = $this->createForm(FormularioCommentsType::class);

       $comentarios->handleRequest($request);


       if ($comentarios->isSubmitted() && $comentarios->getValid()) {
           $em->persist($comentarios);
           $em->flush();

           return $this->redirectToRoute('homepage');
       }

       return $this->render(
           'comentarios.html.twig',
           [
               'comentarios' => $comentarios->createView(),
           ]
       );
   }
}    