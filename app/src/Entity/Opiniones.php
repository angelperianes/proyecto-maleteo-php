<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OpinionesRepository")
 */
class Opiniones
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $Localización;

    /**
     * @ORM\Column(type="text")
     */
    private $Comentario;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getLocalización(): ?string
    {
        return $this->Localización;
    }

    public function setLocalización(string $Localización): self
    {
        $this->Localización = $Localización;

        return $this;
    }

    public function getComentario(): ?string
    {
        return $this->Comentario;
    }

    public function setComentario(string $Comentario): self
    {
        $this->Comentario = $Comentario;

        return $this;
    }
}
